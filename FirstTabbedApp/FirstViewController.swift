//
//  FirstViewController.swift
//  FirstTabbedApp
//
//  Created by Prashant Rizal on 9/25/18.
//  Copyright © 2018 Prashant Rizal. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func changeText(_ sender: Any) {
        label.text="Good job!"
    }
    
}

